////////////////////////////////////////////////
//	Utility (PPS1)
////////////////////////////////////////////////
class graffiti_Caution : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Caution";
    }
}

class graffiti_ArrowUp : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_ArrowUp";
    }
}

class graffiti_ArrowDown : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_ArrowDown";
    }
}

class graffiti_ArrowLeft : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_ArrowLeft";
    }
}

class graffiti_ArrowRight : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_ArrowRight";
    }
}

class graffiti_MonsterCloset : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_MonsterCloset";
    }
}

class graffiti_Crossbones : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Crossbones";
    }
}

class graffiti_No : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_No";
    }
}

class graffiti_QuestionMark : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_QuestionMark";
    }
}

class graffiti_Crosshair : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Crosshair";
    }
}

class graffiti_Treasure : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Treasure";
    }
}

class graffiti_Target : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Target";
    }
}
////////////////////////////////////////////////
//	Tomfoolery (PPS2)
////////////////////////////////////////////////
class graffiti_IceIdiot : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_IceIdiot";
    }
}

class graffiti_TomScott : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_TomScott";
    }
}

class graffiti_GondolaXD : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_GondolaXD";
    }
}

class graffiti_FakeDoorway : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_FakeDoorway";
    }
}
class graffiti_Carl : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Carl";
    }
}

////////////////////////////////////////////////
//	Friends (PPS3)
////////////////////////////////////////////////
class graffiti_Snekhole : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Snekhole";
    }
}
class graffiti_Fella : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Fella";
    }
}
class graffiti_Artiffiti : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_Artiffiti";
    }
}
class graffiti_CluCake : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_CluCake";
    }
}
class graffiti_PointMan : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_PointMan";
    }
}

////////////////////////////////////////////////
//	prettyFist (PPS4)
////////////////////////////////////////////////
class graffiti_SaveTyler : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_SaveTyler";
    }
}

class graffiti_MeltyBlunt : SnekTechSprayerPattern
{
    default
    {
        SnekTechSprayerPattern.decalName "graffiti_MeltyBlunt";
    }
}